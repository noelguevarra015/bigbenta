import 'package:bigbenta/features/bigbenta/domain/entities/BestDealsModel.dart';
import 'package:bigbenta/features/bigbenta/domain/repositories/BigBentaRepository.dart';
import 'package:bigbenta/features/bigbenta/domain/usecases/GetConcreteBestDeals.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockBestDealsRepository extends Mock implements HomePageRepository {}

void main() {
  GetConcreteBestDeals usecase;
  MockBestDealsRepository mockBestDealsRepository;
  Discount discount = Discount(type: null, discount: 0, discountedPrice: null);
  final bestDealsTest = BestDeals(
    id: 54930,
    storeId: 134,
    categoryId: 3,
    categoryName: "Mobile",
    title: 'B100 Optical USB Mouse',
    price: "180.00",
    discount: discount,
    rating: null,
    isMall: false,
    isMart: false,
    primaryImage:
        "https://data.bigbenta.com/storeapp/images/54930/202005280918175ecf11593673d-lg.png",
  );
  setUp(() {
    mockBestDealsRepository = MockBestDealsRepository();
    usecase = GetConcreteBestDeals(mockBestDealsRepository);
  });

  test('should get best deals', () async {
    //arrange
    when(mockBestDealsRepository.getBestDeals(any, any))
        .thenAnswer((_) async => Right(bestDealsTest));
    //act
    //assert
    final result = await usecase(page: 1, limit: 6);
    expect(result, Right(bestDealsTest));
    verify(mockBestDealsRepository.getBestDeals(1, 6));
    verifyNoMoreInteractions(mockBestDealsRepository);
  });
}
