import 'package:bigbenta/core/error/Failures.dart';
import 'package:bigbenta/features/bigbenta/domain/entities/BestDealsModel.dart';
import 'package:bigbenta/features/bigbenta/domain/repositories/BigBentaRepository.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class GetConcreteBestDeals {
  final HomePageRepository repository;

  GetConcreteBestDeals(this.repository);

  Future<Either<Failure, BestDeals>> call(
      {@required int page, @required int limit}) async {
    return await repository.getBestDeals(page, limit);
  }
}
