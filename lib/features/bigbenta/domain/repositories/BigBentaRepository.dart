import 'package:bigbenta/core/error/Failures.dart';
import 'package:bigbenta/features/bigbenta/domain/entities/BestDealsModel.dart';
import 'package:dartz/dartz.dart';

abstract class HomePageRepository {
  Future<Either<Failure, BestDeals>> getBestDeals(int page, int limit);
}
