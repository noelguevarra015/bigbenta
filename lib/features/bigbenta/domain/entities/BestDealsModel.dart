// class BestDealsModel {
//   bool status;
//   List<BestDeals> data;
//
//   BestDealsModel({this.status, this.data});
//
//   BestDealsModel.fromJson(Map<String, dynamic> json) {
//     status = json['status'];
//     if (json['data'] != null) {
//       data = new List<BestDeals>();
//       json['data'].forEach((v) {
//         data.add(new BestDeals.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['status'] = this.status;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

import 'package:equatable/equatable.dart';

class BestDeals extends Equatable {
  int id;
  int storeId;
  String title;
  String primaryImage;
  int categoryId;
  String categoryName;
  String price;
  Discount discount;
  int rating;
  bool isMall;
  bool isMart;
  String productHighlight;
  int soldCount;
  int freeDelivery;

  BestDeals({
    this.id,
    this.storeId,
    this.title,
    this.primaryImage,
    this.categoryId,
    this.categoryName,
    this.price,
    this.discount,
    this.rating,
    this.isMall,
    this.isMart,
    this.productHighlight,
    this.soldCount,
    this.freeDelivery,
  });

  BestDeals.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    title = json['title'];
    primaryImage = json['primary_image'];
    categoryId = json['category_id'];
    categoryName = json['category_name'];
    price = json['price'];
    discount = json['discount'] != null
        ? new Discount.fromJson(json['discount'])
        : null;
    rating = json['rating'];
    isMall = json['is_mall'];
    isMart = json['is_mart'];
    productHighlight = json['product_highlight'];
    soldCount = json['sold_count'];
    freeDelivery = json['free_delivery'];
  }
}

class Discount {
  Null type;
  int discount;
  Null discountedPrice;

  Discount({this.type, this.discount, this.discountedPrice});

  Discount.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    discount = json['discount'];
    discountedPrice = json['discounted_price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['discount'] = this.discount;
    data['discounted_price'] = this.discountedPrice;
    return data;
  }
}

class Sale {
  Null start;
  Null end;

  Sale({this.start, this.end});

  Sale.fromJson(Map<String, dynamic> json) {
    start = json['start'];
    end = json['end'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['start'] = this.start;
    data['end'] = this.end;
    return data;
  }
}

class Store {
  int id;
  int storeId;
  int categoryId;
  String title;
  String description;
  Null brand;
  String price;
  int discount;
  String productHighlights;
  Null discountType;
  int stock;
  String weight;
  String height;
  String length;
  String width;
  Null createdAt;
  String updatedAt;
  Null deletedAt;
  String status;
  Null soldCount;
  int freeDelivery;
  Null startSaleTime;
  Null endSaleTime;

  Store(
      {this.id,
      this.storeId,
      this.categoryId,
      this.title,
      this.description,
      this.brand,
      this.price,
      this.discount,
      this.productHighlights,
      this.discountType,
      this.stock,
      this.weight,
      this.height,
      this.length,
      this.width,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.status,
      this.soldCount,
      this.freeDelivery,
      this.startSaleTime,
      this.endSaleTime});

  Store.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    categoryId = json['category_id'];
    title = json['title'];
    description = json['description'];
    brand = json['brand'];
    price = json['price'];
    discount = json['discount'];
    productHighlights = json['product_highlights'];
    discountType = json['discount_type'];
    stock = json['stock'];
    weight = json['weight'];
    height = json['height'];
    length = json['length'];
    width = json['width'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    status = json['status'];
    soldCount = json['sold_count'];
    freeDelivery = json['free_delivery'];
    startSaleTime = json['start_sale_time'];
    endSaleTime = json['end_sale_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_id'] = this.storeId;
    data['category_id'] = this.categoryId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['brand'] = this.brand;
    data['price'] = this.price;
    data['discount'] = this.discount;
    data['product_highlights'] = this.productHighlights;
    data['discount_type'] = this.discountType;
    data['stock'] = this.stock;
    data['weight'] = this.weight;
    data['height'] = this.height;
    data['length'] = this.length;
    data['width'] = this.width;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['status'] = this.status;
    data['sold_count'] = this.soldCount;
    data['free_delivery'] = this.freeDelivery;
    data['start_sale_time'] = this.startSaleTime;
    data['end_sale_time'] = this.endSaleTime;
    return data;
  }
}
